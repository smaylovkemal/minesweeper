import { render, fireEvent, waitFor } from "@testing-library/react";
import Minesweeper from "components/Minesweeper";

test("render default board 16х30", () => {
  const { container } = render(<Minesweeper />);
  expect(container.getElementsByClassName("cell").length).toBe(16 * 30);
});

test("open cell by click", async () => {
  const { container } = render(<Minesweeper />);
  const cell = container.getElementsByClassName("cell")[0];

  fireEvent.click(cell);

  await waitFor(() => {
    expect(cell).toHaveClass("cell_opened");
  });
});

test("mark cell as flagged by right click", async () => {
  const { container } = render(<Minesweeper />);
  const cell = container.getElementsByClassName("cell")[0];

  fireEvent.contextMenu(cell);

  await waitFor(() => {
    expect(cell).toHaveClass("cell_flagged");
  });
});

test("can not open the flagged cell", async () => {
  const { container } = render(<Minesweeper />);
  const cell = container.getElementsByClassName("cell")[0];

  fireEvent.contextMenu(cell);
  fireEvent.click(cell);

  await waitFor(() => {
    expect(cell).not.toHaveClass("cell_opened");
  });
});
