const gameOver = (state) => {
  const { cells } = state;
  state.isLose = true;
  for (let r = 0; r < cells.length; r++) {
    for (let c = 0; c < cells[0].length; c++) {
      let cell = cells[r][c];
      if (cell.isMine && !cell.isFlagged) {
        state.cells[r][c].isOpened = true;
      }
    }
  }
};

const getMinesCountAround = (state, x, y) => {
  if (state.cells[y][x].isMine) return;

  let count = 0;

  getNeighbors(state, x, y).forEach(({ x, y }) => {
    if (state.cells[y][x].isMine) count++;
  });

  return count;
};

const getNeighbors = (state, x, y) => {
  const neighbors = [];
  for (let r = y - 1; r <= y + 1; r++) {
    for (let c = x - 1; c <= x + 1; c++) {
      if (
        0 <= r &&
        r < state.cells.length &&
        0 <= c &&
        c < state.cells[0].length &&
        !(r === y && c === x)
      ) {
        neighbors.push({ y: r, x: c });
      }
    }
  }
  return neighbors;
};

const isGameFinished = (state) => state.isWon || state.isLose;

const isWon = (cells, minesCount) => {
  const totalCount = cells.length * cells[0].length;
  let openedCount = 0;
  let flaggedCount = 0;

  cells.forEach((row) => {
    row.forEach((cell) => {
      if (cell.isOpened && !cell.isMine) openedCount++;
      if (cell.isFlagged) flaggedCount++;
    });
  });

  return (
    openedCount + flaggedCount === totalCount && minesCount === flaggedCount
  );
};

const openCell = (state, x, y) => {
  const { cells } = state;

  if (cells[y][x].isFlagged || cells[y][x].isOpened) return;

  state.cells[y][x].isOpened = true;

  if (cells[y][x].isMine) {
    gameOver(state);
  } else if (isWon(cells, state.minesCount)) {
    state.isWon = true;
  }

  if (
    isGameFinished(state) ||
    cells[y][x].isMine ||
    cells[y][x].minesCountAround > 0
  )
    return state;

  getNeighbors(state, x, y).forEach(({ x, y }) => {
    openCell(state, x, y);
  });

  return state;
};

export { getMinesCountAround, isGameFinished, isWon, openCell };
