import Cell from "./components/Cell";
import Header from "./components/Header";
import Victory from "./components/Victory";

import styles from "./minesweeper.module.css";

const Game = ({ state, onCellClick, onCellRightClick, onRestart }) => {
  const { cells, isWon } = state;

  return (
    <div className={styles.container}>
      <Header state={state} onRestart={onRestart} />
      <div className={styles.cells}>
        {cells.map((row, i) => (
          <div className={styles.row} key={i}>
            {row.map((cell, j) => (
              <Cell
                key={`${i}${j}`}
                cell={cell}
                x={j}
                y={i}
                onCellClick={onCellClick}
                onCellRightClick={onCellRightClick}
              />
            ))}
          </div>
        ))}
        {isWon && <Victory />}
      </div>
    </div>
  );
};

export default Game;
