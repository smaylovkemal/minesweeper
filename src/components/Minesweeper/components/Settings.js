import { useState } from "react";
import { Button, Modal, Row, Col, Form } from "react-bootstrap";

const Settings = ({ settings, onHide, onSubmit }) => {
  const [cols, setCols] = useState(settings.cols);
  const [rows, setRows] = useState(settings.rows);

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit({ rows: Math.floor(rows), cols: Math.floor(cols) });
  };

  return (
    <Modal show onHide={onHide}>
      <Modal.Header>
        <Modal.Title>Settings</Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <Row>
            <Col xs={6}>
              <Form.Group className="mb-3">
                <Form.Label>Rows:</Form.Label>
                <Form.Control
                  type="number"
                  min="8"
                  max="30"
                  step="1"
                  placeholder="Enter number"
                  required
                  value={rows}
                  onChange={(e) => setRows(Math.floor(e.target.value))}
                />
              </Form.Group>
            </Col>
            <Col xs={6}>
              <Form.Group className="mb-3">
                <Form.Label>Cols:</Form.Label>
                <Form.Control
                  type="number"
                  min="10"
                  max="50"
                  placeholder="Enter number"
                  required
                  value={cols}
                  onChange={(e) => setCols(Math.floor(e.target.value))}
                />
              </Form.Group>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={onHide}>
            Cancel
          </Button>
          <Button variant="primary" type="submit">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default Settings;
