import cn from "classnames";

import { ReactComponent as Flag } from "images/flag.svg";
import { ReactComponent as Bomb } from "images/bomb.svg";

import "./styles.css";

const Number = ({ value }) => {
  if (!value) return null;
  return <span className={`cell_number_${value}`}>{value}</span>;
};

const Cell = ({ cell, x, y, onCellClick, onCellRightClick }) => {
  return (
    <div
      className={cn("cell", {
        cell_mine: cell.isMine && cell.isOpened,
        cell_opened: cell.isOpened,
        cell_flagged: cell.isFlagged,
      })}
      onClick={() => onCellClick(x, y)}
      onContextMenu={(e) => {
        e.preventDefault();
        onCellRightClick(x, y);
      }}
    >
      {cell.isOpened &&
        (cell.isMine ? (
          <Bomb className="bomb" />
        ) : (
          <Number value={cell.minesCountAround} />
        ))}
      {cell.isFlagged && !cell.isOpened && <Flag />}
    </div>
  );
};

export default Cell;
