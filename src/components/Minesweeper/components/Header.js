import { useState } from "react";
import { Button, Row, Col } from "react-bootstrap";

import Settings from "./Settings";

import { ReactComponent as Restart } from "images/restart.svg";
import { ReactComponent as SettingsIcon } from "images/settings.svg";

import styles from "../minesweeper.module.css";

const getMinesLeft = (minesCount, cells) => {
  let flaggedCount = 0;

  cells.forEach((row) => {
    row.forEach((cell) => {
      if (cell.isFlagged) flaggedCount++;
    });
  });

  return Math.max(minesCount - flaggedCount, 0);
};

const Header = ({ state: { minesCount, cells, settings }, onRestart }) => {
  const [modal, setModal] = useState("");

  return (
    <>
      <Row className="align-items-center">
        <Col xs={4} className="text-start">
          Mines:{" "}
          <span className="text-danger fw-bolder">
            {getMinesLeft(minesCount, cells)}
          </span>
        </Col>
        <Col xs={4} className="text-center">
          <Button size="sm" variant="link" onClick={() => onRestart()}>
            <Restart className={styles.header_icon} />
          </Button>
        </Col>
        <Col xs={4} className="text-end">
          <Button size="sm" variant="link" onClick={() => setModal("SETTINGS")}>
            <SettingsIcon className={styles.header_icon} />
          </Button>
        </Col>
      </Row>
      {modal === "SETTINGS" && (
        <Settings
          settings={settings}
          onHide={() => setModal("")}
          onSubmit={(settings) => {
            setModal("");
            onRestart(settings);
          }}
        />
      )}
    </>
  );
};

export default Header;
