import { ReactComponent as WinIcon } from "images/win.svg";
import styles from "../minesweeper.module.css";

const Victory = () => (
  <div className={styles.victory}>
    <WinIcon className={styles.victory_icon} />
    <div className="text-primary fw-bold">You Won!</div>
  </div>
);

export default Victory;
