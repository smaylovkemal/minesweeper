import { Component } from "react";

import Minesweeper from "./Minesweeper";
import {
  isGameFinished,
  openCell,
  getMinesCountAround,
  isWon,
} from "./helpers";
import _ from "lodash";

const initialState = {
  cells: [],
  cellsOpenedCount: 0,
  minesCount: 0,
  minesInited: false,
  isWon: false,
  isLose: false,
  settings: {
    rows: 16,
    cols: 30,
  },
};

class MinesweeperStateContainer extends Component {
  state = initialState;

  componentDidMount() {
    this.initGame();
  }

  initGame = (settings = this.state.settings) => {
    const { rows, cols } = settings;
    const cells = [];

    for (let i = 0; i < settings.rows; i++) {
      let row = [];
      for (let j = 0; j < settings.cols; j++) {
        row.push({
          isFlagged: false,
          isOpened: false,
          isMine: false,
          minesCountAround: 0,
        });
      }
      cells.push(row);
    }

    this.setState({
      ...initialState,
      cells,
      settings,
      minesCount: Math.round(rows * cols * 0.2),
    });
  };

  cellClick = async (x, y) => {
    if (!this.state.minesInited) {
      await this.initMines(x, y);
    }

    const cell = this.state.cells[y][x];

    if (isGameFinished(this.state) || cell.isFlagged) return;

    if (!cell.isOpened && !cell.isFlagged) {
      this.setState((state) => openCell(_.cloneDeep(state), x, y));
    }
  };

  cellRightClick = (x, y) => {
    if (isGameFinished(this.state) || this.state.cells[y][x].isOpened) return;
    const cells = _.cloneDeep(this.state.cells);
    cells[y][x].isFlagged = !cells[y][x].isFlagged;
    this.setState({ cells, isWon: isWon(cells, this.state.minesCount) });
  };

  initMines = (x, y) => {
    return new Promise((resolve) => {
      const {
        settings: { rows, cols },
        minesCount,
        cells,
      } = this.state;
      let mines = minesCount;

      let row, col;
      while (mines) {
        row = Math.floor(Math.random() * rows);
        col = Math.floor(Math.random() * cols);

        if ((row === y && col === x) || cells[row][col].isMine) continue;

        cells[row][col].isMine = true;
        mines--;
      }

      for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
          cells[i][j].minesCountAround = getMinesCountAround(this.state, j, i);
        }
      }

      this.setState({ minesInited: true, cells }, resolve);
    });
  };

  render() {
    return (
      <Minesweeper
        state={this.state}
        onCellClick={this.cellClick}
        onCellRightClick={this.cellRightClick}
        onRestart={(settings) => this.initGame(settings)}
      />
    );
  }
}

export default MinesweeperStateContainer;
